'use strict';

const {
  assert
} = require('chai');
const wd = require('macaca-wd');
require('./wd-extend')(wd, false);
const loginPage=require('../pageObjects/loginPage.js')
const testData=require('../testdata/logindata.json')
var browser ="firefox";
browser = browser.toLowerCase();

describe('macaca-test/desktop-browser-sample.test.js', function() {
  this.timeout(5 * 60 * 1000);

  var driver = wd.promiseChainRemote({
    host: 'localhost',
    port: process.env.MACACA_SERVER_PORT || 3456
  });

  before(() => {
    return driver
      .init({
        platformName: 'desktop',
        browserName: 'chrome',
      })
  });

  describe('strikingly', function() {
    it('#1 should works with web', function() {
      const initialURL = 'https://www.qa.strikingly.com/';
      return driver
        .get(initialURL)
        
        .elementByCssSelector(loginPage.Elements.loginButtonCss)
        
        .click()

        
        
        .elementByCssSelector("#user_email")
        .sendKeys(testData.login.email)
        .elementByCssSelector('#user_password')
        .sendKeys("testtest")
        .elementByCssSelector('[value="Log in"]').click()
        .elementByCssSelector('[ng-click="editButtonClick($event)"]')
        .click()
        //.elementByCssSelector('#tutorial > div > div > div.panel.panel1.active > div.next > a.s-btn.small.dark-gray')
        //.click()
        .elementByCssSelector('div.s-side-menu-btn > a.settings').click()
        .elementByCssSelector('li.domains-item > span.btn-text').click()
        //.elementByCssSelector('[placeholder="e.g. www.mydomain.com"]').click()
        
        .elementByCssSelector('div.permalink-inner > input ')
        .click()
        .clear()
        .sendKeys('sam')
        .elementByCssSelector('div.permalink-update-btn').click()
        .elementByCssSelector('header.page-settings-header >span.close-btn').click()
        .elementByCssSelector('div.s-side-menu-btn > a.page').click()
        .sleep(2000)
        .acceptAlert()
        .sleep(3000)

        .elementByCssSelector('#publish-public-url')
        
        .text()
        .then(value=>{
          console.log("value of text : "+value)
          assert.equal(value,"http://sam.qa.strikingly.com","FAIL: Url does not match")
        })




        //macaca run -d ./macaca-test/desktop-browser-sample.test.js

        

        
        

        

       
        

        
      });



      

  });

  



});
