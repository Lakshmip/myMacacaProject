'use strict';

const {
assert
} = require('chai');
const wd = require('macaca-wd');
require('./wd-extend')(wd, false);

var browser = 'chrome';
browser = browser.toLowerCase();

describe('macaca-test/desktop-browser-sample.test.js', function() {
this.timeout(5 * 60 * 1000);

var driver = wd.promiseChainRemote({
host: 'localhost',
port: process.env.MACACA_SERVER_PORT || 3456
});

before(() => {
return driver
.init({
platformName: 'desktop',
browserName: 'chrome',
})
});

describe('strikingly', function() {
it('#1 should works with web', function() {
const initialURL = 'https://www.qa.strikingly.com/';
return driver
.get(initialURL)
.elementByCssSelector('[href="/s/login?locale=en"]')

.click()

.elementById("user_email")

.sendKeys("rainqa_ls_bright_12@strikingly.com")
.elementByCssSelector('#user_password')
.sendKeys("testtest")
.elementByCssSelector('div.submit >input.s-btn')
.click()
.elementByCssSelector('a.s-btn.edit')
.click()
.elementByCssSelector('div.s-side-menu-btn>a.settings')
.click()

.elementByCssSelector('div.site-title>input')
.click()
.clear()
.sendKeys("Test site of Maha")
.elementByCssSelector('div.form-field  > textarea')
.click()
.clear()
.sendKeys("BE MOTIVATED")

.elementByCssSelector('div.update-btn')
.click()
.elementByCssSelector('header.page-settings-header >span.close-btn')
.click()
.elementByCssSelector('a.publish-button>span.button-label>div.button-label-content')
.click()
.sleep(5000)
.acceptAlert()
.elementByCssSelector('#publish-public-url')
.text()
.then(value=> {
    console.log("value of URL : " +value)
    assert.equal(value,"http://site-37973-11821-97367.qa.strikingly.com","fail: URL not found")
})
});

});
});

//macaca run -d ./macaca-test/desktop-browser-sample.test.js