'use strict';

const {
  assert
} = require('chai');
const wd = require('macaca-wd');
require('./wd-extend')(wd, false);
const loginPage=require('../pageObjects/loginPage.js')
const dashboard=require('../pageObjects/dashboard.js')
const edit=require('../pageObjects/edit.js')
const sitedata=require('../pageObjects/sitedata.js')
const publish=require('../pageObjects/publish.js')
const testData=require('../testdata/logindata.json')
const sitedataa=require('../testdata/sitedataa.json')

var browser ="firefox";
browser = browser.toLowerCase();

describe('macaca-test/desktop-browser-sample.test.js', function() {
  this.timeout(5 * 60 * 1000);

  var driver = wd.promiseChainRemote({
    host: 'localhost',
    port: process.env.MACACA_SERVER_PORT || 3456
  });

  before(() => {
    return driver
      .init({
        platformName: 'desktop',
        browserName: 'chrome',
      })
  });

  describe('LOGIN', function() {
    it('#1 should works with web', function() {
      const initialURL = 'https://www.qa.strikingly.com/';
      return driver
        .get(initialURL)
        
        .elementByCssSelector(loginPage.Elements.loginButtonCss)
        
        .click()
        .elementByCssSelector(loginPage.Elements.emailByCss)
        .click()
        .sendKeys(testData.login.email)
        .elementByCssSelector(loginPage.Elements.passwordByCss)
    
        .sendKeys(testData.login.passwrd)
        .elementByCssSelector(loginPage.Elements.log)
        .click()
        .elementByCssSelector(loginPage.Elements.value)
        .text()
        .then(value=>{
          console.log("value of text : "+value)
          assert.equal(value,"LIMITED","LIMITED IS NOT FOUND")
        });

    });

        it('dashboard', function() {
          
          return driver
        .sleep(5000)
        .elementByCssSelector(dashboard.Elements.editID)
        .click()
         });
    


        it('setting', function() {
         return driver
        .elementByCssSelector(edit.Elements.settings)
        .click()
        .elementByCssSelector(edit.Elements.tour)
        .text()
        .then(value=>{
          console.log("value of text : "+value)
          assert.equal(value,"TAKE TOUR","TAKE TOUR IS NOT FOUND")
        });
        
          });
    
    it('basic info', function() {
      return driver
      .elementByCssSelector(edit.Elements.basicinfo)
      .click()
      .elementByCssSelector(edit.Elements.analytics)
        .text()
        .then(value=>{
          console.log("value of text : "+value)
          assert.equal(value,"ANALYTICS","ANALYTICS IS NOT FOUND")
        });


    })
    it('site', function() {
      return driver
      .elementByCssSelector(sitedata.Elements.sitetitle)
      .click()
      .clear()
      .sendKeys(sitedataa.site.sitetitleedit)
      .elementByCssSelector(sitedata.Elements.update)
      .click()
      .elementByCssSelector(sitedata.Elements.close)
      .click()
      .elementByCssSelector(sitedata.Elements.category)
        .text()
        .then(value=>{
          console.log("value of text : "+value)
          assert.equal(value,"Category","Category IS NOT FOUND")
        });
    })
    it('publish', function() {
      return driver
      .sleep(5000)
      .elementByCssSelector(publish.Elements.publishit)
      .click()
      .sleep(5000)
      .acceptAlert()
      .sleep(2000)
      .elementByCssSelector(publish.Elements.fb)
      
        .text()
        .then(value=>{
          console.log("value of text : "+value)
          assert.equal(value,"SHARE ON FACEBOOK"," IS NOT FOUND")
        });

      



    })

});
});